extends PanelContainer

signal button_pressed


onready var label = $MarginContainer/VBoxContainer/Label
onready var button = $MarginContainer/VBoxContainer/Button


func set_text(text : String) -> void:
	label.text = text


func set_button_text(text : String) -> void:
	button.text = text


func _on_Button_pressed():
	emit_signal("button_pressed")


