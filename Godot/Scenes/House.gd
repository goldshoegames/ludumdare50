extends Node2D


func _ready():
	var portal_spawns := []
	for child in get_children():
		if child is PortalSpawnPoint:
			portal_spawns.append(child.global_position)
	var portal_position : Vector2 = portal_spawns[randi() % portal_spawns.size()]
	var portal = preload("res://Scenes/Portal.tscn").instance()
	portal.position = portal_position
	add_child(portal)
	_spawn_time_police()
	_show_popup()
	if Global.house_number > 1:
		$AudioStreamPlayer.play()


func _process(delta):
	Global.time_survived += delta / 60.0


func _show_popup() -> void:
	var popup = preload("res://Scenes/Popup.tscn").instance()
	match Global.house_number:
		1:
			popup.set_text("I need to find that 1 minute time portal I left laying around...")
		2:
			popup.set_text("Great, the time police are here.")
		4:
			popup.set_text("What the hell, the house changed?")
		5:
			popup.set_text("Time portals must be messing with reality...")
		6:
			popup.set_text("What psycho version of me lives here?")
		_:
			popup.queue_free()
			return
	$UiLayer.add_child(popup)


func _spawn_time_police() -> void:
	if Global.house_number > 1:
		var police_scene = preload("res://Scenes/BadMan.tscn")
		var police_count : int = Global.house_number - 1
		for i in police_count:
			var spawn_position := Vector2(40 + (randf() * 568), 28 + (randf() * 280))
			var police = police_scene.instance()
			police.position = spawn_position
			add_child(police)


func get_time_left() -> float:
	return $UiLayer/Clock.seconds / 60.0


func _on_Clock_time_up():
	get_tree().change_scene("res://Scenes/EndScreen.tscn")


