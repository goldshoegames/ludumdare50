extends KinematicBody2D

class_name GoodMan


onready var sprite = $Sprite
onready var portal_seer = $PortalSeer
onready var raycast = $RayCast2D

var speed : float = 100

var invincible : bool = true


func _physics_process(delta):
	var dir = Vector2.ZERO
	if Input.is_action_pressed("move_up"):
		dir.y -= 1
	if Input.is_action_pressed("move_down"):
		dir.y += 1
	if Input.is_action_pressed("move_left"):
		dir.x -= 1
	if Input.is_action_pressed("move_right"):
		dir.x += 1
	if dir != Vector2.ZERO:
		var translation : Vector2 = dir.normalized() * speed * delta
		move_and_slide(dir.normalized() * speed)
		sprite.rotation = translation.angle()


func _update_portal_visibility(portal : TimePortal) -> void:
	raycast.cast_to = portal.global_position - global_position
	raycast.force_raycast_update()
	if raycast.is_colliding():
		portal.hide_portal()
	else:
		portal.show_portal()


func _on_PortalSeer_area_entered(area):
	if area is TimePortal:
		_update_portal_visibility(area)


func _on_PortalSeer_area_exited(area):
	if area is TimePortal:
		(area as TimePortal).hide_portal()


func _on_PortalCollider_area_entered(area):
	if area is TimePortal:
		Global.house_number += 1
		Global.time_survived += get_parent().get_time_left()
		get_tree().change_scene_to(Global.get_next_house())


func _on_PortalCheckTimer_timeout():
	for area in portal_seer.get_overlapping_areas():
		if area is TimePortal:
			_update_portal_visibility(area)


func _on_InvincibilityTimer_timeout():
	invincible = false


