extends CenterContainer


signal time_up

onready var label = $Label

var seconds : float = 60

func _process(delta):
	seconds = max(seconds-delta, 0)
	if seconds > 59:
		label.text = "1:00"
	elif seconds > 10:
		label.text = "0:%s" % ceil(seconds)
	else:
		label.text = "0:0%s" % ceil(seconds)
	if seconds <= 0:
		emit_signal("time_up")


