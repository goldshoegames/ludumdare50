extends Area2D


var velocity := Vector2.ZERO


func _physics_process(delta):
	translate(velocity * delta)
	


func _on_Bullet_body_entered(body):
	if body is GoodMan:
		if !(body as GoodMan).invincible:
			get_tree().change_scene("res://Scenes/EndScreen.tscn")
	else:
		queue_free()


func _on_Timer_timeout():
	queue_free()
