extends Area2D

class_name TimePortal

onready var sprite = $Sprite

var spin_speed : float = PI


func _ready():
	sprite.visible = false


func _process(delta):
	sprite.rotate(spin_speed * delta)


func show_portal() -> void:
	sprite.visible = true


func hide_portal() -> void:
	sprite.visible = false


