extends KinematicBody2D


export var bullet_scene : PackedScene

onready var detector = $GoodManDetector
onready var sprite = $Sprite
onready var cooldown_timer = $CooldownTimer

var speed = 50
var bullet_speed = 100
var wander_velocity := Vector2(speed, 0).rotated(randf() * TAU)
var shot_ready : bool = true


func _physics_process(delta):
	if detector.get_overlapping_bodies().size() > 0:
		var goodman = detector.get_overlapping_bodies()[0]
		var dir = (goodman.global_position - global_position).normalized()
		sprite.rotation = dir.angle()
		if shot_ready:
			shot_ready = false
			cooldown_timer.start()
			var bullet = bullet_scene.instance()
			bullet.position = position
			bullet.velocity = dir * bullet_speed
			get_parent().add_child(bullet)
			$AudioStreamPlayer2D.play()
	else:
		move_and_slide(wander_velocity)
		sprite.rotation = wander_velocity.angle()



func _on_Timer_timeout():
	wander_velocity = Vector2(speed, 0).rotated(randf() * TAU)


func _on_CooldownTimer_timeout():
	shot_ready = true


