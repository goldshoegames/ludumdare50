extends Node2D

onready var text_box = $TextBox



func _ready():
	text_box.set_text("You managed to live %s minutes during the last minute of Earth." % Global.time_survived)
	text_box.set_button_text("Goodbye World")


func _on_TextBox_button_pressed():
	get_tree().change_scene("res://Scenes/StartScreen.tscn")

