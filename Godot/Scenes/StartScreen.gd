extends Node2D


onready var text_box = $TextBox


func _ready():
	Global.time_survived = 0
	Global.house_number = 1
	randomize()
	var bad_candidates = ["Canadians", "Americans", "Hawaiians", "Rabbits", "Aliens", "Redheads", "Robots", "Hippies", "Swiss"]
	var baddies : String = bad_candidates[randi() % bad_candidates.size()]
	text_box.set_text("This is it. Those damn %s have finally lauched the nukes. But I'm going to squeeze as much time out of Earth's last minute as possible." % baddies)


func _on_TextBox_button_pressed():
	get_tree().change_scene("res://Scenes/House1.tscn")


