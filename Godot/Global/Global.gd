extends Node


var time_survived : float = 0
var house_number : int = 1

const houses := [
	preload("res://Scenes/House1.tscn"),
	preload("res://Scenes/House2.tscn"),
	preload("res://Scenes/House3.tscn"),
	preload("res://Scenes/House4.tscn")
]


func get_next_house() -> PackedScene:
	if house_number < 4:
		return houses[0]
	var index = (house_number-3) % 4
	return houses[index]


